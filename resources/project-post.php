<?php
if(get_field('thumbnail')) {
    $thumbnail = get_field('thumbnail');
} else {
    $thumbnail = get_stylesheet_directory_uri() . "/assets/images/placeholder.jpg";
}
?>
<div class="col-12 col-sm-6 post-wrap">
    <figure class="post-item project-item">
        <img src="<?php echo $thumbnail; ?>" class="img-fluid" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
        <figcaption>
            <h2><?php the_title(); ?></h2>
            <p><?php echo wp_trim_words(get_the_content(), 18, '&hellip;' ); ?></p>
            <a href="<?php the_permalink() ?>"></a>
        </figcaption>
    </figure>
</div>