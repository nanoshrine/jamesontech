export default {
    init() {
        // JavaScript to be fired on all pages
        jQuery(document).ready(function ($) {
            var ajaxUrl = window.location.origin + '/wp-admin/admin-ajax.php';
            //blog
            var blog_btn = $('.post-load-more-button.blog');
            var blog_area = $('.blog-area');
            var blog_filter = $('.blog-filter');
            var blog_page = 1;
            var blog_ppp = 4;
            //project
            var project_btn = $('.post-load-more-button.project');
            var project_area = $('.project-area');
            var project_filter = $('.project-filter');
            var project_page = 1;
            var project_ppp = 4;

            function resetBlogPage() {
                blog_page = 1;
            }

            function simulateBlogLoad() {
                blog_btn.addClass('button--loading');
                blog_btn.find('span').text('Loading...');
            }

            blog_filter.click(function () {
                if (!$(this).hasClass('selected')) {
                    blog_filter.removeClass('selected');
                    $(this).addClass('selected');
                    var cat = $(this).attr('data-filter');
                    blog_btn.addClass('button--hide');
                    resetBlogPage();
                    blog_area.fadeOut('slow', function () {
                        $.post(ajaxUrl, {
                            action: 'more_posts_ajax',
                            blog_offset: 0,
                            blog_ppp: blog_ppp,
                            blog_category: cat
                        }).success(function (posts) {
                            if (posts) {
                                blog_area.html(posts).hide().fadeIn();
                                blog_btn.removeClass('button--hide');
                            }
                        }).fail(function (xhr) {
                            alert(xhr.responseText);
                        });
                    });
                }
            });


            blog_btn.on('click', function () {
                simulateBlogLoad();
                var blog_category = $('.blog-filter.selected').attr('data-filter');
                $.post(ajaxUrl, {
                    action: 'more_posts_ajax',
                    blog_offset: (blog_page * blog_ppp),
                    blog_ppp: blog_ppp,
                    blog_category: blog_category
                }).success(function (posts) {
                    if (posts) {
                        blog_page++;
                        $(posts).hide().appendTo('.blog-area').fadeIn();
                        blog_btn.removeClass('button--loading');
                        blog_btn.find('span').text('Load More');
                    } else {
                        blog_btn.removeClass('button--loading');
                        blog_btn.find('span').text('Load More');
                        blog_btn.addClass('button--hide');
                    }
                }).fail(function (xhr) {
                  //  alert(xhr);
                });
            });


            function resetProjectPage() {
                project_page = 1;
            }

            function simulateProjectLoad() {
                project_btn.addClass('button--loading');
                project_btn.find('span').text('Loading...');
            }

            project_filter.click(function () {
                if (!$(this).hasClass('selected')) {
                    project_filter.removeClass('selected');
                    $(this).addClass('selected');
                    var cat = $(this).attr('data-filter');
                    project_btn.addClass('button--hide');
                    resetProjectPage();
                    project_area.fadeOut('slow', function () {
                        $.post(ajaxUrl, {
                            action: 'more_projects_ajax',
                            project_offset: 0,
                            project_ppp: project_ppp,
                            project_category: cat
                        }).success(function (posts) {
                            if (posts) {
                                project_area.html(posts).hide().fadeIn();
                                project_btn.removeClass('button--hide');
                            }
                        }).fail(function (xhr) {
                            alert(xhr.responseText);
                        });
                    });
                }
            });

            project_btn.on('click', function () {
                simulateProjectLoad();
                var project_category = $('.project-filter.selected').attr('data-filter');
                $.post(ajaxUrl, {
                    action: 'more_projects_ajax',
                    project_offset: (project_page * project_ppp),
                    project_ppp: project_ppp,
                    project_category: project_category
                }).success(function (posts) {
                    if (posts) {
                        project_page++;
                        $(posts).hide().appendTo('.project-area').fadeIn();
                        project_btn.removeClass('button--loading');
                        project_btn.find('span').text('Load More');
                    } else {
                        project_btn.removeClass('button--loading');
                        project_btn.find('span').text('Load More');
                        project_btn.addClass('button--hide');
                    }
                }).fail(function (xhr) {
                 //   alert(xhr.responseText);
                });
            });

            if (window.location.href.indexOf("?done") > -1) {
                window.location.href = "/";
            }

        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired


    },
};
