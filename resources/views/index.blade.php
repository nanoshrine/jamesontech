@extends('layouts.secondary')

@section('hero')
    @include('includes.hero.blog-hero')
@endsection

@section('content')
    @include('partials.page-header')
    <div class="row grid post-area">
        @while (have_posts()) @php(the_post())
        @php
            $thumbnail = get_field('thumbnail');
        @endphp
        <div class="col-12 col-sm-6 post-wrap">
            <figure class="post-item">
                <img src="{{ $thumbnail }}" class="img-fluid" alt="{{ the_title() }}"/>
                <figcaption>
                    <h2>{{ the_title() }}</h2>
                    <p>{!! wp_trim_words(get_the_content(), 18, '&hellip;' ) !!}</p>
                    <a href="{{ the_permalink() }}"></a>
                </figcaption>
            </figure>
        </div>
        @endwhile
    </div>
    <div class="pagination-wrap">
    @php
        the_posts_pagination( array(
            'prev_text'          => __( 'Previous page', 'cm' ),
            'next_text'          => __( 'Next page', 'cm' ),
            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'cm' ) . ' </span>',
        ) );
    @endphp
    </div>
@endsection
