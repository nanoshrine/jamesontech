{{--
  Template Name: Homepage
--}}

@extends('layouts.app')

@section('content')
    <div class="ct" id="home">
        <div class="ct" id="about">
            <div class="ct" id="projects">
                <div class="ct" id="blog">
                    @include('partials.menu')
                    <div class="page home" id="p1">
                        @include('includes.home')
                    </div>
                    <div class="page about" id="p2">
                        @include('includes.about')
                    </div>
                    <div class="page projects" id="p3">
                        @include('includes.projects')
                    </div>
                    <div class="page blog" id="p4">
                        @include('includes.blog')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
