<ul id="menu">
    <a href="#home">
      <li class="icon" id="uno"><img class="menu-icon" src="@asset('images/home.svg')"></li>
    </a>

    <a href="#about">
      <li class="icon fi flaticon-023-user-1" id="dos"></li> 
    </a>

    <a href="#projects">
        <li class="icon" id="tres"><img class="menu-icon" src="@asset('images/projects.svg')"></li>
    </a>

    <a href="#blog">
        <li class="icon" id="cuatro"><img class="menu-icon" src="@asset('images/blog.svg')"></li>
    </a>
</ul>
