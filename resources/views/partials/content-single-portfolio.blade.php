@php
    $id = get_the_ID();
    $thumbnail = get_the_post_thumbnail_url($id);
    $view_link = get_field('view_link');
    $view_title = get_field('view_title');
    $source_link = get_field('source_link');
    $source_title = get_field('source_title');
    $label_link = get_field('label_link');
    $label_name = get_field('label_name');
    $extra_links = get_field('extra_links');
    $sc_link = get_field('sc_link');
    $itunes_link = get_field('itunes_link');
    $spotify_link = get_field('spotify_link');
    $apple_link = get_field('apple_link');
    $download_link = get_field('download_link');
    $release_date = get_field('release_date');
    $bc_embed = get_field('bc_embed');
    $bc_embed_video = get_field('bc_embed_video');
    $yt_embed = get_field('yt_embed');
    $itch_link = get_field('itch_link');
    $steam_link = get_field('steam_link');
    $m4c = get_field('m4c');
@endphp
<article @php(post_class())>
    <div class="img-wrap">
        <div class="shadow-box"></div>
        @if($bc_embed_video)
            <iframe class="bc-video" style="border: 0; width: 100%; height: 549px;"
                    src="{{ $bc_embed_video }}" mozallowfullscreen="1"
                    webkitallowfullscreen="1" allowfullscreen="1" seamless></iframe>
        @elseif($bc_embed)
            <iframe style="border: 0; width: 100%; max-width: 700px; margin: 0 auto; margin-top: 30px; display: block; height: 472px;"
                    src="{{ $bc_embed }}/artwork=small/transparent=true/" seamless></iframe>
        @elseif($yt_embed)
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $yt_embed }}"></iframe>
          </div>
        @else
            <img src="{{ $thumbnail }}" class="img-fluid">
        @endif
    </div>
    <header>
        <h1 class="entry-title text-center">{{ get_the_title() }}</h1>
        <div class="text-center">
            @include('partials/entry-meta')
        </div>
        <div class="post-info text-center">
            @if($view_link)
                <a href="{{ $view_link }}" target="_blank" class="btn-main"><span
                            data-hover="{{ $view_title }}">View Live</span></a>
            @endif
            @if($source_link)
                <a href="{{ $source_link }}" target="_blank" class="btn-main"><span
                            data-hover="{{ $source_title }}">View Source</span></a>
            @endif
            @if($label_link)
                <a href="{{ $label_link }}" target="_blank" class="btn-main"><span
                            data-hover="{{ $label_name }}"><b>Released:</b> {{ $release_date }}</span></a>
            @endif
            @if($download_link)
                <a href="{{ $download_link }}" target="_blank" class="btn-main"><span
                            data-hover="Bandcamp">Download</span></a>
            @endif
            @if($steam_link)
                <a href="{{ $steam_link }}" target="_blank" class="btn-main steam"><span
                            data-hover="View"><img src="@asset('images/steam-icon.png')"> Steam</span></a>
            @endif
            @if($itch_link)
                <a href="{{ $itch_link }}" target="_blank" class="btn-main itch"><span
                            data-hover="View"><img src="@asset('images/itch-icon.svg')"> Itch.io</span></a>
            @endif
            @if($extra_links)
                <div class="music-links">
                    @if($sc_link)
                        <a href="{{ $sc_link }}" target="_blank"><img src="@asset('images/soundcloud.svg')"></a>
                    @endif
                    @if($itunes_link)
                        <a href="{{ $itunes_link }}" target="_blank"><img src="@asset('images/itunes.svg')"></a>
                    @endif
                    @if($spotify_link)
                        <a href="{{ $spotify_link }}" target="_blank"><img src="@asset('images/spotify.svg')"></a>
                    @endif
                    @if($apple_link)
                        <a href="{{ $apple_link }}" target="_blank"><img src="@asset('images/apple.svg')"></a>
                    @endif
                </div>
            @endif
            @if($m4c)
              <div class="credits">
                <p>Created with the team at:</p>
                <a href="https://marketingforchange.com" target="_blank">
                  <img src="@asset('images/m4c-logo.png')">
                </a>
              </div>
            @endif
        </div>
    </header>
    <div class="divider div-transparent div-dot"></div>
    <div class="entry-content">
        @php(the_content())
    </div>
    <footer>
        {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    </footer>
    @php(comments_template('/partials/comments.blade.php'))
</article>

@php
    wp_reset_postdata();
@endphp

<div class="divider div-transparent div-dot"></div>
<div class="row">
    <div class="col-12 text-center">
        <h3>Recent Projects</h3>
    </div>
</div>
<div class="row grid more-posts project">
    @php
        $args = [
                    'post_type' => 'portfolio',
                    'post__not_in' => [$id],
                    'order'            => 'DESC',
                    'posts_per_page'   => 2,
                 ];
        $post_query = new WP_Query($args);
        if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post();
        $thumbnail = get_field('thumbnail');
    @endphp

    <div class="col-12 col-sm-6 post-wrap">
        <figure class="post-item project-item">
            <img src="{{ $thumbnail }}" class="img-fluid" alt="{{ the_title() }}"/>
            <figcaption>
                <h2>{{ the_title() }}</h2>
                <p>{!! wp_trim_words(get_the_content(), 15, '&hellip;' ) !!}</p>
                <a href="{{ the_permalink() }}"></a>
            </figcaption>
        </figure>
    </div>
    @php endwhile; else : @endphp
    <h4 class="text-center">No Projects Found</h4>
    @php endif; wp_reset_postdata(); @endphp
    <div class="col-12 text-center view-more">
        <a href="{{ site_url() }}#projects" class="btn-main"><span data-hover="Click Me!">View All Projects</span></a>
    </div>
</div>
