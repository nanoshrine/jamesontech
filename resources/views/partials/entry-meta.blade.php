<?php $nodate = get_field('no_date'); ?>
@if (!$nodate)
  <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
@endif
<p class="byline author vcard">
  {{ __('By', 'Jameson') }} <a href="{{ site_url() }}#about" rel="author" class="fn">
    {{ get_the_author() }}
  </a>
</p>
