<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116489980-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-116489980-1');
  </script>

  @if(is_front_page())
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Tech Entrepreneur and Frequency Manipulator" />
    <meta property="og:description" content="I&#039;m a seasoned software developer and musician who thrives on complex projects focusing on Web and Virtual Reality development." />
    <meta property="og:url" content="https://jameson.tech/" />
    <meta property="og:site_name" content="Jameson.Tech" />
    <meta property="og:image" content="https://jameson.tech/wp-content/uploads/2019/08/new_fb.jpg" />
    <meta property="og:image:secure_url" content="https://jameson.tech/wp-content/uploads/2018/03/fb-share.jpg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="I&#039;m a seasoned software developer and musician who thrives on complex projects focusing on Web and Virtual Reality development." />
    <meta name="twitter:title" content="Tech Entrepreneur and Frequency Manipulator" />
    <meta name="twitter:image" content="https://jameson.tech/wp-content/uploads/2019/08/new_tw.jpg" />
  @endif

  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5aa34baff117926f"></script>
  @php(wp_head())
</head>
