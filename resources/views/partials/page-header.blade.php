<div class="page-header text-center">
    <a href="#a" class="btn-main" onclick='javascript:event.preventDefault();history.go(-1)'><span data-hover="Take Me Back!">Go Back</span></a>
    <a href="{{ site_url() }}/#blog" class="btn-main">
        <i class="fa fa-chevron-left"></i><span data-hover="Return To Blog">Blog</span>
    </a>
    <h1>{!! App::title() !!}</h1>
</div>
