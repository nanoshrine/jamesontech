@php
    $id = get_the_ID();
    $thumbnail = get_the_post_thumbnail_url($id);
    $youtube = get_field('youtube_link');
    $tags = get_the_tags();
@endphp
<article @php(post_class())>
    <div class="img-wrap">
        <div class="shadow-box"></div>
        @if($youtube)
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//www.youtube.com/embed/{{ $youtube }}"></iframe>
            </div>
        @else
            <img src="{{ $thumbnail }}" class="img-fluid">
        @endif
    </div>
    <header>
        <h1 class="entry-title text-center">{{ get_the_title() }}</h1>
        <div class="text-center">
            @include('partials/entry-meta')
        </div>
    </header>
    <div class="divider div-transparent div-dot"></div>
    <div class="entry-content">
        @php(the_content())
        @if($tags)
            <div class="tag-area">
                @foreach ($tags as $tag)
                    <a href="{{ site_url() . '/tag/' . $tag->slug }}" class="btn-tag">{{ $tag->name }}</a>
                @endforeach
            </div>
        @endif
    </div>
    <footer>
        {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    </footer>
    @php(comments_template('/partials/comments.blade.php'))
</article>

@php
    wp_reset_postdata();
@endphp

<div class="divider div-transparent div-dot"></div>
<div class="row">
    <div class="col-12 text-center">
        <h3>Recent Posts</h3>
    </div>
</div>
<div class="row grid more-posts blog">
    @php
        $args = [
                    'post_type' => 'post',
                    'post__not_in' => [$id],
                    'order'            => 'DESC',
                    'posts_per_page'   => 2,
                 ];
        $post_query = new WP_Query($args);
        if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post();
        $thumbnail = get_field('thumbnail');
    @endphp

    <div class="col-12 col-sm-6 post-wrap">
        <figure class="post-item blog-item">
            <img src="{{ $thumbnail }}" class="img-fluid" alt="{{ the_title() }}"/>
            <figcaption>
                <h2>{{ the_title() }}</h2>
                <p>{!! wp_trim_words(get_the_content(), 15, '&hellip;' ) !!}</p>
                <a href="{{ the_permalink() }}"></a>
            </figcaption>
        </figure>
    </div>
    @php endwhile; else : @endphp
    <h3 class="text-center">No Posts Found</h3>
    @php endif; wp_reset_postdata(); @endphp
    <div class="col-12 text-center view-more">
        <a href="{{ site_url() }}#blog" class="btn-main"><span data-hover="Click Me!">View All Posts</span></a>
    </div>
</div>