<section class="icon home">
  <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r79/three.min.js"></script>
    <script src="https://pixijs.download/dev-graphics-batch-pool/pixi.js"></script>
<script src="https://cdn.jsdelivr.net/gh/T-vK/SuperParticles@master/SuperParticles.js"></script>
    <script src="@asset('scripts/vanta.js')"></script>
    <script>
        VANTA.WAVES({
            el: "#p1",
            color: 0x290031,
                shininess: 74.00,
                waveSpeed: 0.85,
                zoom: .70
        });
    </script>
    <div class="home-text">
        <svg class="main" viewBox="0 0 1280 720">
            <text text-anchor="middle" x="50%" y="50%">Jameson.tech</text>
        </svg>
    </div>
    <svg class="sub-text" width="50%" height="100%" viewBox="30 -50 600 500" xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
        <path id="path">
            <animate attributeName="d" from="m0,110 h0" to="m0,110 h1100" dur="5.4s" begin="0s"
                     repeatCount="indefinite"/>
        </path>
        <text font-size="60" font-family="Alice" fill='hsla(360, 100%, 100%, 1)' data-text=".Programming &nbsp;.Music &nbsp;.Crypto">
            <textPath xlink:href="#path">.VR &nbsp;.AR &nbsp;.Web &nbsp;.UE4 &nbsp;.Music &nbsp;.Crypto</textPath>
        </text>
    </svg>

    <div class="svg-wrapper">
        <a id="jump" href="#projects">
          <svg height="60" width="320" xmlns="http://www.w3.org/2000/svg">
        <rect class="shape" height="60" width="320" />
        <div class="text">View Projects</div>
        </svg></a>
      </div>


</section>
