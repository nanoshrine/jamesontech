<div id="particles-background3" class="vertical-centered-box neat-background"></div>
<div id="particles-foreground3" class="vertical-centered-box neat-foreground"></div>
<section class="icon">
    <img class="page-icon" src="@asset('images/blog.svg')">
    <h2>Blog</h2>
    <p>Scroll to reveal more posts</p>
    <div class="row filter-area">
        <div class="col-12">
            @php
                $cat_args = array(
                              'hide_empty' => true,
                              'order'    => 'ASC',
                           );
            $categories = get_categories($cat_args);
            @endphp
            <button class="blog-filter btn-filter selected"
                    data-filter="">All
            </button>
            @foreach($categories as $category)
                <button class="blog-filter btn-filter"
                        data-filter="{{ $category->slug }}">{{ get_cat_name($category->term_id) }} </button>
            @endforeach
        </div>
    </div>
    <div class="blog-area-wrap">
        <div class="row grid post-area blog-area">
            @php
                $blogcounter = 0;
                $args = [
                            'post_type' => 'post',
                            'order'            => 'DESC',
                            'posts_per_page'   => 4,
                         ];
                $post_query = new WP_Query($args);
                if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post();
                $thumbnail = get_field('thumbnail');
                $blogcounter++;
            @endphp

            <div class="col-12 col-sm-6 post-wrap">
                <figure class="post-item blog-item">
                    @if($thumbnail)
                        <img src="{{ $thumbnail }}" class="img-fluid" alt="{{ the_title() }}" title="{{ the_title() }}"/>
                    @else
                        <img src="@asset('images/placeholder.jpg')" class="img-fluid" alt="placeholder" title="placeholder"/>
                    @endif
                    <figcaption>
                        <h2>{{ the_title() }}</h2>
                        <p>{!! wp_trim_words(get_the_content(), 18, '&hellip;' ) !!}</p>
                        <a href="{{ the_permalink() }}"></a>
                    </figcaption>
                </figure>
            </div>
            @php endwhile; else : @endphp
            <h3 class="text-center">No Posts Found</h3>
            @php endif; wp_reset_postdata(); @endphp
        </div>
        <button class="post-load-more-button blog @if($blogcounter < 4) hidden @endif">
            <div class="post-btn-loader button__loader"></div>
            <span>Load More</span>
        </button>
    </div>
</section>
