<div id="particles-background2" class="vertical-centered-box neat-background"></div>
<div id="particles-foreground2" class="vertical-centered-box neat-foreground"></div>
<section class="icon">
    <img class="page-icon" src="@asset('images/projects.svg')">
    <h2>Projects</h2>
    <p>Scroll to reveal more projects</p>
    <div class="row filter-area">
        <div class="col-12">
            @php
                $categories = get_terms( array(
                    'taxonomy' => 'portfolio_categories',
                    'hide_empty' => true,
                ));
            @endphp
            <button class="project-filter btn-filter selected"
                    data-filter="">All
            </button>
            @foreach($categories as $category)
                <button class="project-filter btn-filter"
                        data-filter="{{ $category->slug }}">{{ $category->name }} </button>
            @endforeach
        </div>
    </div>
    <div class="project-area-wrap">
        <div class="row grid post-area project-area">
            @php
                $args = [
                            'post_type' => 'portfolio',
                            'order'            => 'DESC',
                            'posts_per_page'   => 4,
                         ];
                $post_query = new WP_Query($args);
                if ($post_query->have_posts()) : while ($post_query->have_posts()) : $post_query->the_post();
                $thumbnail = get_field('thumbnail');
            @endphp
            <div class="col-12 col-sm-6 post-wrap">
                <figure class="post-item project-item">
                    @if($thumbnail)
                        <img src="{{ $thumbnail }}" class="img-fluid" alt="{{ the_title() }}" title="{{ the_title() }}"/>
                    @else
                        <img src="@asset('images/placeholder.jpg')" class="img-fluid" alt="placeholder" title="placeholder"/>
                    @endif
                    <figcaption>
                        <h2>{{ the_title() }}</h2>
                        <p>{!! wp_trim_words(get_the_content(), 18, '&hellip;' ) !!}</p>
                        <a href="{{ the_permalink() }}"></a>
                    </figcaption>
                </figure>
            </div>
            @php endwhile; else : @endphp
            <h3 class="text-center">No Projects Found</h3>
            @php endif; wp_reset_postdata(); @endphp
        </div>
        <button class="post-load-more-button project">
            <div class="post-btn-loader button__loader"></div>
            <span>Load More</span>
        </button>
    </div>
</section>
