<section class="post-header project-hero">
    <div class="logo">
        <a href="{{ site_url() }}"><img class="menu-icon" src="@asset('images/home.svg')" alt="home" title="home"></a><br>
        <a href="{{ site_url() }}#about" class="back"><i class="fi flaticon-023-user-1"></i></a>
        <a href="{{ site_url() }}#projects" class="back"><img class="menu-icon"
                                                              src="@asset('images/projects.svg')" alt="projects" title="projects"></a>
        <a href="{{ site_url() }}#blog" class="back"><img class="menu-icon"
                                             src="@asset('images/blog.svg')" alt="blog" title="blog"></a>
    </div>
    <div class="hero-image-container">
        <div class="window-background"></div>
        <div class="moon"></div>
        <img class="hero-image" src="@asset('images/project-hero.svg')"
             alt="animations">
        <div class="screen-container"></div>
        <div class="notification"></div>
    </div>
</section>