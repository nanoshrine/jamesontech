<div id="particles-background" class="vertical-centered-box neat-background"></div>
<div id="particles-foreground" class="vertical-centered-box neat-foreground"></div>
<section class="icon">
   <div id="particles-js" class="particles-bg"></div>
    <div class="comparison-slider-wrapper">
        <div class="comparison-slider">
            <img src="@asset('images/right-image.jpg')" alt="Nanoshrine" title="Nanoshrine">
            <div class="resize">
                <img src="@asset('images/left-image.jpg')" alt="Jameson" title="Jameson">
            </div>
            <div class="divider"></div>

        </div>
    </div>
    <h2>About Jameson</h2>
    <div class="social-links">
        <a href="https://www.linkedin.com/in/jamesontucker/" target="_blank"><img
                    src="@asset('images/linkedin.svg')" alt="linkedin" title="linkedin"></a>
        <a href="https://twitter.com/nanoshrine" target="_blank"><img src="@asset('images/twitter.svg')" alt="twitter" title="twitter"></a>
        <a href="https://github.com/jamesontucker" target="_blank"><img class="github"
                                                                        src="@asset('images/github.svg')" alt="github" title="github"></a>
        <a href="https://soundcloud.com/nanosmusics" target="_blank"><img src="@asset('images/soundcloud.svg')" alt="soundcloud" title="soundcloud"></a>
        <a href="https://nanoshrine.bandcamp.com" target="_blank"><img src="@asset('images/bandcamp.png')" alt="bandcamp" title="bandcamp"></a>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="bio-wrap">
                @php
                    $page = get_page_by_title( 'Home' );
                    $about = get_field('about_section', $page->ID);
                @endphp
                {!! $about !!}
            </div>
        </div>
    </div>
    <div class="divider div-transparent div-dot"></div>
    <div class="row">
        <div class="contact">
            <h3>Have a question for me?</h3>
            {!! do_shortcode( '[pirate_forms]' ) !!}
        </div>
    </div>
</section>
