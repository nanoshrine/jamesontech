@extends('layouts.secondary')

@section('hero')
    @if(get_post_type() === 'portfolio')
        @include('includes.hero.project-hero')
    @else
        @include('includes.hero.blog-hero')
    @endif
@endsection

@section('content')
    <div class="single">
        @while(have_posts()) @php(the_post())
        @include('partials.content-single-'.get_post_type())
        @endwhile
    </div>
@endsection